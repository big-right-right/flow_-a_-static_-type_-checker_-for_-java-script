# Flow_A_Static_Type_Checker_ for_JavaScript 123

## 介绍
Flow (一个 JavaScript 静态类型检测器)应用小例

## Flow开发工具插件 (Flow Language Support)
在vscode中安装插件 Flow Language Support，则不符合类型检查的代码都会被标记出来

## 安装和检查
1.  通过如下步骤安装 flow-bin:
    执行 cnpm install flow-bin --save-dev
2.  通过如下步骤在项目根目录下添加一个 .flowconfig 文件:
    执行 yarn flow init

    .flowconfig 文件内容如下：
    [ignore]

    [include]

    [libs]

    [lints]

    [options]

    [strict]

3.  在源代码文件夹src下，在需要进行静态类型检测的文件首行，添加 // @flow
4.  通过如下步骤对文件进行静态类型检查：
    执行 yarn flow

    此时会看到控制台，输出了类型检查错误。


## 移除源代码中的类型标记
由于带上flow类型检查标记的文件，不是纯正的js文件，无法正常执行，所以需要移除源代码中的类型标记。

### 用 flow-remove-types 移除源码中的flow类型标记
1.  通过如下步骤安装 flow-remove-types：
    执行 cnpm install flow-remove-types --save-dev
2.  通过如下步骤把源码编译为无标记的代码：
    执行 yarn flow-remove-types src -d dist

    此时，就会在目标代码文件夹dist中，发现已经移除了类型标记的代码文件。

### 用 babel 移除源码中的flow类型标记
1.  通过如下步骤安装 babel:
    执行 cnpm install @babel/core @babel/cli @babel/preset-flow --save-dev
2.  在项目根目录下添加babel的配置文件 .babelrc
    .babelrc文件内容如下：
    {
        "presets": ["@babel/preset-flow"]
    }
3.  通过如下步骤把源码编译为无标记的代码：
    执行 yarn babel src -d dist2


## Flow类型参考
1.Flow官网类型描述文档

2.第三方的类型手册：
https://www.saltycrane.com/cheat-sheets/flow-type/latest



